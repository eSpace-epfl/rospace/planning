Planning repository
====================

[![pipeline status](https://gitlab.com/eSpace-epfl/rospace/planning/badges/master/pipeline.svg)](https://gitlab.com/eSpace-epfl/rospace/planning/commits/master)
[![codecov](https://codecov.io/gl/eSpace-epfl:rospace/planning/branch/master/graph/badge.svg)](https://codecov.io/gl/eSpace-epfl:rospace/planning)

This repository holds modules such as:

* Offline or online maneuver planners
* Collision avoidance planners
* Capture planners

and so on.
