import unittest
import sys
import os
import yaml

sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + "/../src/")  # hack...
sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + "/../../../rdv-cap-sim/src/")

from state import *
from checkpoint import *
from orbit_adjuster import *

from rospace_lib.misc import *

ABSOLUTE_TEST = "test_abs"
RELATIVE_TEST = "test_rel"
DRIFT_TEST = "test_drift"

TARGET_SPACECRAFT = "planning_target"
CHASER_SPACECRAFT = "planning_chaser"


class OrbAdjTest(unittest.TestCase):
    """Test orbit adjusters.

    In case tests should fail check if the position tolerance of the propagator is
    set low enough (lower than 0.1). High tolerance don't provide enough precision
    for these tests and cause failure.
    """

    def setUp(self):
        """Set up tests.

        Load the initial conditions configuration file and stores the 3 test conditions
        absolute, relative and drift.
        """
        # load configuration file
        abs_path = os.path.dirname(os.path.abspath(__file__))
        initial_conditions_path = os.path.join(abs_path, '../cfg/initial_conditions.yaml')
        initial_conditions_file = file(initial_conditions_path, 'r')
        initial_conditions = yaml.load(initial_conditions_file)

        # store initial conditions of all tests
        self.epoch_abs = eval(initial_conditions[ABSOLUTE_TEST]['oe_epoch'])
        self.init_coords_abs = initial_conditions[ABSOLUTE_TEST]['init_coords']

        self.epoch_rel = eval(initial_conditions[RELATIVE_TEST]['oe_epoch'])
        self.init_coords_rel = initial_conditions[RELATIVE_TEST]['init_coords']

        self.epoch_drift = eval(initial_conditions[DRIFT_TEST]['oe_epoch'])
        self.init_coords_drift = initial_conditions[DRIFT_TEST]['init_coords']

    def test_arg_of_perigee(self):
        """Test argument of perigee orbit adjuster.

        Initial conditions: 'init_coords_abs'
        Propagator type: '2-body'

        Add a disturbance to the argument of perigee and try to correct it, to see
        if the maneuver perform correctly with 2-body propagator.
        """
        target = Spacecrafts.Planning_Spacecraft(TARGET_SPACECRAFT)
        chaser = Spacecrafts.Planning_Spacecraft(CHASER_SPACECRAFT, chaser=True)
        propagator_type = "2-body"

        # needed for orbit propagation:
        OrekitPropagator.init_jvm()
        FileDataHandler.load_magnetic_field_models(self.epoch_abs)

        chaser = PropagatorParser.parse_configuration_files(
            chaser, self.init_coords_abs["chaser"], self.epoch_abs)
        target = PropagatorParser.parse_configuration_files(
            target, self.init_coords_abs["target"], self.epoch_abs)

        target.build_propagator(self.epoch_abs, propagator_type)
        chaser.build_propagator(self.epoch_abs, propagator_type, parent_state=target.abs_state)

        orb_adj = ArgumentOfPerigee()

        # Test 1 - Increase argument of perigee
        checkpoint1 = AbsoluteCP()
        checkpoint1.set_abs_state(chaser.get_mean_oe())
        checkpoint1.abs_state.w += 0.1

        orb_adj.evaluate_manoeuvre(chaser, checkpoint1, target)

        chaser_mean = chaser.get_mean_oe()

        self.assertAlmostEqual(checkpoint1.abs_state.a, chaser_mean.a, 4)
        self.assertAlmostEqual(checkpoint1.abs_state.e, chaser_mean.e, 4)
        self.assertAlmostEqual(checkpoint1.abs_state.i, chaser_mean.i, 4)
        self.assertAlmostEqual(checkpoint1.abs_state.O, chaser_mean.O, 4)

        # Test 2 - Decrease argument of perigee
        checkpoint2 = AbsoluteCP()
        checkpoint2.set_abs_state(chaser.get_mean_oe())
        checkpoint2.abs_state.w -= 0.1

        orb_adj.evaluate_manoeuvre(chaser, checkpoint2, target)

        chaser_mean = chaser.get_mean_oe()

        self.assertAlmostEqual(checkpoint2.abs_state.a, chaser_mean.a, 4)
        self.assertAlmostEqual(checkpoint2.abs_state.e, chaser_mean.e, 4)
        self.assertAlmostEqual(checkpoint2.abs_state.i, chaser_mean.i, 4)
        self.assertAlmostEqual(checkpoint2.abs_state.O, chaser_mean.O, 4)

    def test_plane_adjustment(self):
        """Test plane change orbit adjuster.

        Initial conditions: 'init_coords_abs'
        Propagator type: '2-body'

        Check if the plane adjustment perform a correct manoeuvre in case of
        2-body propagation.
        """
        target = Spacecrafts.Planning_Spacecraft(TARGET_SPACECRAFT)
        chaser = Spacecrafts.Planning_Spacecraft(CHASER_SPACECRAFT, chaser=True)
        propagator_type = "2-body"

        # needed for orbit propagation:
        OrekitPropagator.init_jvm()
        FileDataHandler.load_magnetic_field_models(self.epoch_abs)

        chaser = PropagatorParser.parse_configuration_files(
            chaser, self.init_coords_abs["chaser"], self.epoch_abs)
        target = PropagatorParser.parse_configuration_files(
            target, self.init_coords_abs["target"], self.epoch_abs)

        target.build_propagator(self.epoch_abs, propagator_type)
        chaser.build_propagator(self.epoch_abs, propagator_type, parent_state=target.abs_state)

        orb_adj = PlaneOrientation()

        # Test 1 - Increase both inclination and RAAN
        checkpoint1 = AbsoluteCP()
        checkpoint1.set_abs_state(chaser.get_mean_oe())
        checkpoint1.abs_state.i += 0.1
        checkpoint1.abs_state.O += 0.1

        orb_adj.evaluate_manoeuvre(chaser, checkpoint1, target)

        chaser_mean = chaser.get_mean_oe()

        self.assertAlmostEqual(checkpoint1.abs_state.a, chaser_mean.a, 4)
        self.assertAlmostEqual(checkpoint1.abs_state.e, chaser_mean.e, 4)
        self.assertAlmostEqual(checkpoint1.abs_state.i, chaser_mean.i, 4)
        self.assertAlmostEqual(checkpoint1.abs_state.O, chaser_mean.O, 4)

        # Test 2 - Decrease both inclination and RAAN
        checkpoint2 = AbsoluteCP()
        checkpoint2.set_abs_state(chaser.get_mean_oe())
        checkpoint2.abs_state.i -= 0.1
        checkpoint2.abs_state.O -= 0.1

        orb_adj.evaluate_manoeuvre(chaser, checkpoint2, target)

        chaser_mean = chaser.get_mean_oe()

        self.assertAlmostEqual(checkpoint2.abs_state.a, chaser_mean.a, 4)
        self.assertAlmostEqual(checkpoint2.abs_state.e, chaser_mean.e, 4)
        self.assertAlmostEqual(checkpoint2.abs_state.i, chaser_mean.i, 4)
        self.assertAlmostEqual(checkpoint2.abs_state.O, chaser_mean.O, 4)

        # Test 3 - Increase inclination and decrease RAAN
        checkpoint3 = AbsoluteCP()
        checkpoint3.set_abs_state(chaser.get_mean_oe())
        checkpoint3.abs_state.i += 0.1
        checkpoint3.abs_state.O -= 0.1

        orb_adj.evaluate_manoeuvre(chaser, checkpoint3, target)

        chaser_mean = chaser.get_mean_oe()

        self.assertAlmostEqual(checkpoint3.abs_state.a, chaser_mean.a, 4)
        self.assertAlmostEqual(checkpoint3.abs_state.e, chaser_mean.e, 4)
        self.assertAlmostEqual(checkpoint3.abs_state.i, chaser_mean.i, 4)
        self.assertAlmostEqual(checkpoint3.abs_state.O, chaser_mean.O, 4)

        # Test 4 - Decrease inclination and increase RAAN
        checkpoint4 = AbsoluteCP()
        checkpoint4.set_abs_state(chaser.get_mean_oe())
        checkpoint4.abs_state.i -= 0.1
        checkpoint4.abs_state.O += 0.1

        orb_adj.evaluate_manoeuvre(chaser, checkpoint4, target)

        chaser_mean = chaser.get_mean_oe()

        self.assertAlmostEqual(checkpoint4.abs_state.a, chaser_mean.a, 4)
        self.assertAlmostEqual(checkpoint4.abs_state.e, chaser_mean.e, 4)
        self.assertAlmostEqual(checkpoint4.abs_state.i, chaser_mean.i, 4)
        self.assertAlmostEqual(checkpoint4.abs_state.O, chaser_mean.O, 4)

    def test_multi_lambert(self):
        """Test multi-lambert orbit adjuster.

        Initial conditions: 'init_coords_rel'
        Propagator type: '2-body'

        Check if the multi-lambert perform a correct manoeuvre in case of
        2-body propagation.
        """
        target = Spacecrafts.Planning_Spacecraft(TARGET_SPACECRAFT)
        chaser = Spacecrafts.Planning_Spacecraft(CHASER_SPACECRAFT, chaser=True)
        propagator_type = "2-body"

        # needed for orbit propagation:
        OrekitPropagator.init_jvm()
        FileDataHandler.load_magnetic_field_models(self.epoch_abs)

        chaser = PropagatorParser.parse_configuration_files(
            chaser, self.init_coords_rel["chaser"], self.epoch_rel)
        target = PropagatorParser.parse_configuration_files(
            target, self.init_coords_rel["target"], self.epoch_rel)

        target.build_propagator(self.epoch_abs, propagator_type)
        chaser.build_propagator(self.epoch_abs, propagator_type, parent_state=target.abs_state)

        checkpoint = RelativeCP()
        checkpoint.rel_state.R = np.array([0.0, 1.0, 0.0])
        checkpoint.rel_state.V = np.array([0.0, 0.0, 0.0])
        checkpoint.t_min = 7979
        checkpoint.t_max = 7980

        orb_adj = MultiLambert()
        orb_adj.evaluate_manoeuvre(chaser, checkpoint, target)

        self.assertLess(abs(chaser.rel_state.R[0] - checkpoint.rel_state.R[0]), 1e-2)
        self.assertLess(abs(chaser.rel_state.R[1] - checkpoint.rel_state.R[1]), 1e-2)
        self.assertLess(abs(chaser.rel_state.R[2] - checkpoint.rel_state.R[2]), 1e-2)

    def test_clohessy_wiltshire(self):
        """Test clohessy-wiltshire orbit adjuster.

        Initial conditions: 'init_coords_rel'
        Propagator type: '2-body'

        Check if the clohessy-wiltshire perform a correct manoeuvre in case of
        2-body propagation.
        """
        target = Spacecrafts.Planning_Spacecraft(TARGET_SPACECRAFT)
        chaser = Spacecrafts.Planning_Spacecraft(CHASER_SPACECRAFT, chaser=True)
        propagator_type = "2-body"

        # needed for orbit propagation:
        OrekitPropagator.init_jvm()
        FileDataHandler.load_magnetic_field_models(self.epoch_abs)

        chaser = PropagatorParser.parse_configuration_files(
            chaser, self.init_coords_rel["chaser"], self.epoch_rel)
        target = PropagatorParser.parse_configuration_files(
            target, self.init_coords_rel["target"], self.epoch_rel)

        target.build_propagator(self.epoch_abs, propagator_type)
        chaser.build_propagator(self.epoch_abs, propagator_type, parent_state=target.abs_state)

        checkpoint = RelativeCP()
        checkpoint.rel_state.R = np.array([0.0, 1.0, 0.0])
        checkpoint.rel_state.V = np.array([0.0, 0.0, 0.0])
        checkpoint.t_min = 7979
        checkpoint.t_max = 7980

        orb_adj = ClohessyWiltshire()
        orb_adj.evaluate_manoeuvre(chaser, checkpoint, target)

        self.assertLess(abs(chaser.rel_state.R[0] - checkpoint.rel_state.R[0]), 1e-2)
        self.assertLess(abs(chaser.rel_state.R[1] - checkpoint.rel_state.R[1]), 1e-2)
        self.assertLess(abs(chaser.rel_state.R[2] - checkpoint.rel_state.R[2]), 1e-2)

    def test_drift(self):
        """Test drifting algorithm.

        Initial conditions: 'init_coords_drift'
        Propagator type: '2-body'

        Check if the drifting algorithm perfom a correct manoeuvre in case of 2-body
        propagation.
        """
        target = Spacecrafts.Planning_Spacecraft(TARGET_SPACECRAFT)
        chaser = Spacecrafts.Planning_Spacecraft(CHASER_SPACECRAFT, chaser=True)
        propagator_type = "2-body"

        # needed for orbit propagation:
        OrekitPropagator.init_jvm()
        FileDataHandler.load_magnetic_field_models(self.epoch_abs)

        chaser = PropagatorParser.parse_configuration_files(
            chaser, self.init_coords_drift["chaser"], self.epoch_drift)
        target = PropagatorParser.parse_configuration_files(
            target, self.init_coords_drift["target"], self.epoch_drift)

        target.build_propagator(self.epoch_abs, propagator_type)
        chaser.build_propagator(self.epoch_abs, propagator_type, parent_state=target.abs_state)

        checkpoint = RelativeCP()
        checkpoint.rel_state.R = np.array([-4.0, 8.0, 0.0])
        checkpoint.rel_state.V = np.array([0.0, 0.0, 0.0])
        checkpoint.error_ellipsoid = np.array([0.2, 0.5, 0.2])

        orb_adj = Drift()
        orb_adj.evaluate_manoeuvre(chaser, checkpoint, target, [])

        self.assertLess(abs(chaser.rel_state.R[0] - checkpoint.rel_state.R[0]), 1e-2)
        self.assertLess(abs(chaser.rel_state.R[1] - checkpoint.rel_state.R[1]), 1e-2)
        self.assertLess(abs(chaser.rel_state.R[2] - checkpoint.rel_state.R[2]), 1e-2)


if __name__ == '__main__':
    unittest.main()
