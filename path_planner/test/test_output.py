import unittest
import sys
import os

sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + "/../src/")  # hack...
sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + "/../../../rdv-cap-sim/src/")

import offline_path_planner
from rospace_lib import KepOrbElem


class OutputTest(unittest.TestCase):
    """
        Test output of the simulation using some predefined scenario
    """

    def test_sample_absolute(self):
        """Test output using scenario_sample_absolute.yaml and using standard initial condition (std_ic).

        Note that this test is performed using 2-body propagator.
        Check if the total deltav and the final position match the following:
            Total DeltaV: 0.018327823164056397 km/s
            Final desired position: [7065.4 0.000810129258075 1.71935390223 0.533974758001 0.396508063906]
            (True anomaly is neglected as it is not important where Hohmann Transfer ends)
        """
        dvtot, ch_final = offline_path_planner.main('scenario_sample_absolute', 'std_ic')

        self.assertAlmostEqual(dvtot, 0.018327823164056397, 4)
        final_state = KepOrbElem()
        final_state.from_cartesian(ch_final.abs_state)

        self.assertAlmostEqual(final_state.a, 7065.4, delta=0.01)
        self.assertAlmostEqual(final_state.e, 0.000810129258075, delta=0.00001)
        self.assertAlmostEqual(final_state.i, 1.71935390223, delta=0.00001)
        self.assertAlmostEqual(final_state.O, 0.396508063906, delta=0.00001)
        self.assertAlmostEqual(final_state.w, 0.533974758001, delta=0.00001)

    def test_sample_relative(self):
        """Test output using scenario_sample_relative.yaml and using standard initial condition (std_ic).

        Note that this test is performed using 2-body propagator.
        Check if the total deltav and the final position match the following:
            Total DeltaV: 0.0258886779442494 km/s
            Final relative position: [0.0 18.0 0.0] km
        """

        dvtot, ch_final = offline_path_planner.main('scenario_sample_relative', 'std_ic')

        self.assertAlmostEqual(dvtot, 0.0258886779442494, 4)
        self.assertAlmostEqual(ch_final.rel_state.R[0], 0.0, delta=0.01)
        self.assertAlmostEqual(ch_final.rel_state.R[1], 18.0, delta=0.01)
        self.assertAlmostEqual(ch_final.rel_state.R[2], 0.0, delta=0.01)


if __name__ == '__main__':
    unittest.main()
